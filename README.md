# Field-effect measurements with source-measure units and PPMS

## Requirements
* Python 3.5 or later
* [NI-VISA](http://sine.ni.com/psp/app/doc/p/id/psp-411)

### Required python libraries (install via `pip`, `conda` etc.)
* slave
* pyvisa
* matplotlib
* PyQt5
* pandas

## Installation & Usage

* `$ pip install https://bitbucket.org/t-onoz/fieldeffect2/get/master.zip`
* `$ fieldeffect`

### Installed scripts
* `fieldeffect`: run field-effect measurement app
* `fieldeffect-noconsole`: same as above, but without console
* `fieldeffect-processdata`: process data with current inversion
* `fieldeffect-editor`: sequence editor without making any connection to instruments
* `fieldeffect-test`: run automated tests of all commands
* `fieldeffect-test-b2900`: run automated tests of Agilent B2900 driver
* `fieldeffect-test-k2400`: run automated tests of Keithley Model 2400 driver

## Supported instruments
* PPMS Model 6000 temperature controller
* Agilent B2902A dual-channel source-measure unit
* Keithley Model 2400 series source-measure unit
* Keithley Model 2000 digital multimeter
* Keithley Model 2182 nanovoltmeter (not tested)

## License
[GNU General Public License v3.0](http://www.gnu.org/licenses/gpl.html)