# -*- coding: utf-8 -*-
import glob
from logging import getLogger
from os import path
from io import StringIO
from PyQt5 import uic

logger = getLogger(__name__)


def compile_ui(force=False):
    processed = False
    for uifile in glob.glob(path.join(path.dirname(__file__), 'fieldeffect/qt/widgets/ui/*.ui')):
        pyfile = path.splitext(uifile)[0] + '.py'
        if force or (not path.exists(pyfile)) or (path.getmtime(uifile) > path.getmtime(pyfile)):
            processed = True
            logger.info('processing %s' % path.basename(uifile))
            with StringIO() as buffer, open(pyfile, 'w', encoding='utf-8') as py:
                uic.compileUi(uifile, buffer, execute=True)
                text = buffer.getvalue()
                text = text.replace('\nfrom PARENT.', '\nfrom ..')
                text = text.replace('\nfrom CURRENT.', '\nfrom .')
                py.write(text)
    if not processed:
        logger.info('no ui files to compile')


if __name__ == '__main__':
    import sys
    import logging
    logger.setLevel(logging.INFO)
    logger.addHandler(logging.StreamHandler())
    force = '--force' in sys.argv
    compile_ui(force)
