# -*- coding: utf-8 -*-
import sys
import time
import re
from typing import List
from logging import getLogger

import numpy as np
import slave.types
from slave import transport


from . import driver, config, linuxgpib
from .utils import flatten


logger = getLogger(__name__)


def scpifloat(array_like):
    fa = np.array(array_like, dtype=float)
    fa[np.isclose(fa, 9.91e37)] = np.nan  # Not a number
    fa[np.isclose(fa, 9.90e37)] = np.inf
    fa[np.isclose(fa, -9.90e37)] = -np.inf
    return fa


def _get_TH(ppms: driver.PPMS):
    try:
        return ppms._query(('GETDAT? 6', (slave.types.Integer, slave.types.Float, slave.types.Float, slave.types.Float)))[2:]
    except Exception:
        logger.error('Error getting temperature and field. Clear the device and try again.', exc_info=True)
        ppms.clear()
        return ppms._query(('GETDAT? 6', (slave.types.Integer, slave.types.Float, slave.types.Float, slave.types.Float)))[2:]


def _simfetch(length):
    """emulates fetch() for SimulatedTransport"""
    return [np.nan for _ in range(length)]


class SecondChannel(object):
    def __init__(self, dual_smu):
        self._smu = dual_smu  # type: driver.B2900
        self.source = self._smu.sources[1]
        self.sense = self._smu.senses[1]
        self.arm = self._smu.arms[1]
        self.triggering = self._smu.triggerings[1]
        self.output = self._smu.outputs[1]
        self.identification = self._smu.identification

    def clear(self):
        return self._smu.clear()

    def wait_to_continue(self):
        return self._smu.wait_to_continue()

    def fetch(self):
        return self._smu.fetch((2,))

    def initiate(self):
        return self._smu.initiate((2,))


class InstrumentBundle(object):
    def __init__(self):
        self.ppms = driver.PPMS(transport.SimulatedTransport())
        self.sd = driver.B2900(transport.SimulatedTransport())
        self.gate = driver.B2900(transport.SimulatedTransport())
        self.voltmeters = []  # type: List[driver.K2000]
        self.setup()

    def connect(self, addrPpms=None, addrSD=None, addrGate=None, *addrVoltmeters, transport_cls=None):
        """connect to various instruments. Then calls self.setup() to apply default settings.

        :param addrPpms: PPMS (e.g. GPIB0::15::INSTR, LinuxGpib-15)
        :param addrSD: B2902A (or Keithley 2400) source-measure unit used as Source-Drain (e.g. GPIB0::23::INSTR)
        :param addrGate: ignored. reserved for future use (see comments in the code)
        :param addrVoltmeters: Optional addresses of Keithley Model 2000 voltmeters
        :param transport_cls: Transport class (automatically inferred from address if None)
        """
        Transport = transport_cls if transport_cls else (lambda addr: self.infer_transport(addr))
        if addrPpms is not None:
            tr = Transport(addrPpms)
            if isinstance(tr, transport.Visa):
                tr._instrument.clear()
            elif hasattr(tr, 'clear'):
                tr.clear()
            self.ppms = driver.PPMS(tr)
        if addrSD is not None:
            self.sd = self.infer_driver(Transport(addrSD), mode='smu')
            if (addrGate is None) or (addrGate == addrSD):
                if getattr(self.sd, 'is_dual', False):
                    # ゲート電極にB2902Aのチャンネル2を使用
                    self.gate = SecondChannel(self.sd)
        if (addrGate is not None) and (addrGate != addrSD):
            # ゲート電極に二台目のソースメジャーユニットを使用
            self.gate = self.infer_driver(Transport(addrGate))
        self.voltmeters = [self.infer_driver(Transport(addr), mode='voltmeter') for addr in addrVoltmeters]
        self.setup()

    def setup(self):
        self.sd.sense_elements = ('voltage', 'current', 'resistance')
        self.sd.sense.enable_all_functions()
        self.sd.sense.resistance.mode = 'manual'
        self.sd.sense.voltage.nplc = config.NPLC
        self.sd.sense.current.nplc = config.NPLC
        self.sd.triggering.delay = config.TRIGGER_DELAY
        if isinstance(self.sd._transport, transport.SimulatedTransport):
            self.sd.fetch = lambda *_, **__: _simfetch(3)

        self.gate.triggering.delay = config.TRIGGER_DELAY
        self.gate.sense.enable_all_functions()
        self.gate.sense.resistance.mode = 'manual'
        self.gate.sense_elements = ('voltage', 'current', 'resistance')
        self.gate.source.mode = 'voltage'
        self.gate.sense.voltage.nplc = config.NPLC
        self.gate.sense.current.nplc = config.NPLC
        self.gate.sense.current.compliance = config.IG_LIMIT
        if isinstance(getattr(self.gate, '_transport', None), transport.SimulatedTransport):
            self.gate.fetch = lambda *_, **__: _simfetch(3)

        for voltmeter in self.voltmeters:
            voltmeter.sense.function = 'voltage'
            voltmeter.initiate.continuous_mode = False
            voltmeter.triggering.delay = config.TRIGGER_DELAY
            voltmeter.triggering.count = 1
            if hasattr(voltmeter.sense, 'voltage'):
                voltmeter.sense.voltage.nplc = config.NPLC
            voltmeter.sense.nplc = config.NPLC
            if isinstance(voltmeter._transport, transport.SimulatedTransport):
                voltmeter.fetch = lambda *_, **__: _simfetch(1)

    def clear_all(self):
        for d in (self.ppms, self.sd, self.gate) + tuple(self.voltmeters):
            if hasattr(d, 'clear') and not isinstance(getattr(d, '_transport', None), transport.SimulatedTransport):
                try:
                    d.clear()
                except Exception:
                    logger.error('Could not clear %s' % d, exc_info=True)

    def measure(self) -> List[float]:
        t = time.time()

        self.sd.output.state = True
        if isinstance(self.gate, SecondChannel):
            # ゲート電極がソースドレインとおなじSMU
            channels = (1, 2) if self.gate.output.state else (1,)
            self.sd.initiate(channels)
            self.sd.wait_to_continue()
        else:
            # 別々の装置
            self.sd.initiate()
            self.gate.initiate()
            self.sd.wait_to_continue()
            self.gate.wait_to_continue()

        for voltmeter in self.voltmeters:
            voltmeter.initiate()
            voltmeter.wait_to_continue()

        data_time = [t]
        data_ppms = list(_get_TH(self.ppms))
        data_sd = list(self.sd.fetch())
        data_gate = list(self.gate.fetch())
        data_voltmeter = list(flatten(v.fetch() for v in self.voltmeters))

        return scpifloat(data_time + data_ppms + data_sd + data_gate + data_voltmeter)

    def get_header(self):
        t = ['time']
        ppms = ['temperature', 'field']
        sd = ['V_SD', 'I_SD', 'R_SD']
        gate = ['V_G', 'I_G', 'R_G']
        volts = ['voltmeter%d' % i for i in range(len(self.voltmeters))]
        return t + ppms + sd + gate + volts

    def measure_bipolar(self):
        if self.sd.source.mode.lower().startswith('volt'):
            subsource = self.sd.source.voltage
        else:
            subsource = self.sd.source.current
        ret0 = self.measure()
        subsource.level_triggered *= -1
        ret1 = self.measure()
        subsource.level *= -1
        subsource.level_triggered *= -1
        return ret0, ret1

    @staticmethod
    def infer_driver(tr: transport.Transport, mode='smu') -> driver.Driver:
        tr.write(b'*IDN?')
        maker, model, serial, revision = tr.read_bytes(1024).decode().strip().split(',')
        if mode == 'smu':
            if re.search(r'B29[0-9][0-9]', model):
                tr.write(b':SYST:LANG?')
                lang = tr.read_bytes(256).decode()
                if lang == '2400':
                    logger.info('Changing B290X system language to DEFault.')
                    tr.write(b':SYST:LANG DEF')
                    raise RuntimeError('Wait for B290X to reboot. Then restart the app.')
                else:
                    return driver.B2900(tr)
            if re.search(r'MODEL 245[0-9]', model):
                tr.write(b'*lang?')
                lang = tr.read_bytes(256).decode().lower()
                if lang != 'scpi2400':
                    raise RuntimeError('Model 2450: Set SCPI language to SCPI2400 from the front panel. Then restart the SMU and app.')
            tr.write(b':OUTP:INT:STAT OFF')
            return driver.K2400(tr)
        elif mode == 'voltmeter':
            if re.search(r'MODEL 2000', model):
                # digital multimeter
                return driver.K2000(tr)
            if re.search(r'MODEL 2182', model):
                # nanovoltmeter
                return driver.K2182(tr)
            return driver.ProtoVoltmeter(tr)
        else:
            raise ValueError('"mode" must be "smu" or "voltmeter"')

    @staticmethod
    def infer_transport(address, *args, **kwargs) -> transport.Transport:
        """Returns instance of Transport object. At present it only supports LinuxGpib and Visa.

        :param address: transport address
        :param args: extra arguments to Transport class
        :param kwargs: extra keyword arguments to Transport class"""
        linuxgpib_header = 'LinuxGpib-'
        if 'linux' in sys.platform:
            if str(address).isnumeric():
                return linuxgpib.LinuxGpib(int(address), *args, **kwargs)
            if str(address).startswith(linuxgpib_header):
                return linuxgpib.LinuxGpib(int(str(address).replace(linuxgpib_header, '')), *args, **kwargs)
        return transport.Visa(address, *args, **kwargs)

    def get_info(self):
        info = []
        info.append(('ppms', self.ppms.__class__, getattr(self.ppms, 'identification', None)))
        info.append(('source-drain', self.sd.__class__, getattr(self.sd, 'identification', None)))
        info.append(('gate', self.gate.__class__, getattr(self.gate, 'identification', None)))
        for idx, v in enumerate(self.voltmeters):
            info.append(('voltmeter%d' % idx, v.__class__, getattr(v, 'identification', None)))
        return info
