# -*- coding: utf-8 -*-
import sys
import os
import traceback
import logging.handlers
from os import path
from logging import getLogger


logger = getLogger(__name__)
dbgfile = path.expanduser('~/.fieldeffect/debug.log')


def excepthook(type_, value, tb):
    if issubclass(type_, KeyboardInterrupt):
        sys.__excepthook__(type_, value, tb)
        return
    logger.critical("Uncaught exception", exc_info=(type_, value, tb))
    import tkinter
    from tkinter import messagebox
    tkinter.Tk().withdraw()
    msg = """Unhandled exception occurred.
--------------------------------
%s--------------------------------
Please ask developer for details.
Application log may be saved in %s.
""" % (''.join(traceback.format_exception(type_, value, tb)), dbgfile)
    messagebox.showerror("Error", msg)
    # QtCore.qFatal("")


def setup_logging(package_logger=None):
    os.makedirs(path.dirname(dbgfile), exist_ok=True)
    if package_logger:
        package_logger.setLevel('DEBUG')
    fh = logging.handlers.RotatingFileHandler(
        dbgfile,
        maxBytes=1024*1024,
        backupCount=2
    )
    fh.setLevel(logging.DEBUG)
    fh.setFormatter(logging.Formatter(
        fmt='*'*80 + '\n' + '%(asctime)s\n%(levelname)s:%(name)s:%(message)s',
        datefmt='%Y-%m-%d %H:%M:%S',
    ))
    getLogger().addHandler(fh)
    getLogger().addHandler(logging.StreamHandler())


def main():
    sys.excepthook = excepthook

    from PyQt5 import QtWidgets
    import fieldeffect
    from fieldeffect import config
    from fieldeffect.measurement import InstrumentBundle
    from .widgets.mainwindow import MainWindow
    from .widgets.connectiondialog import ConnectionDialog

    setup_logging(fieldeffect.logger)
    app = QtWidgets.QApplication(sys.argv)
    logger.info('Starting app')
    dialog = ConnectionDialog()
    if dialog.exec_() != QtWidgets.QDialog.Accepted:
        return
    config.INVERT = dialog.ui.cbInvert.isChecked()
    logger.info('Current inversion: %s', config.INVERT)
    if not dialog.connected:
        bundle = InstrumentBundle()
    else:
        bundle = dialog.bundle

    m = MainWindow(bundle, parent=None)
    m.show()

    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
