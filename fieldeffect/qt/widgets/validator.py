#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from PyQt5 import QtWidgets, QtCore, QtGui


class ValLineEdit(QtWidgets.QLineEdit):
    """Change background color according to validator"""
    color_invalid = QtGui.QColor.fromRgb(246, 152, 157)
    color_valid = QtCore.Qt.white

    def __init__(self, *a, **kw):
        super(ValLineEdit, self).__init__(*a, **kw)
        self.textChanged.connect(self.adapt_color)

    def adapt_color(self):
        color = self.color_valid if self.hasAcceptableInput() else self.color_invalid
        p = self.palette()
        p.setColor(QtGui.QPalette.Base, color)
        self.setAutoFillBackground(True)
        self.setPalette(p)

