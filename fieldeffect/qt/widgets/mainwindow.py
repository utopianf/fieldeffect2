# -*- coding: utf-8 -*-
import traceback
from logging import getLogger
from PyQt5 import QtWidgets, QtCore

from fieldeffect.measurement import InstrumentBundle
from ..comrunner import DataHandler, CommandRunner
from .ui.ui_mainwindow import Ui_MainWindow
from ..misc import catcher

logger = getLogger(__name__)


class MainWindow(QtWidgets.QMainWindow):
    def __init__(self, bundle=None, parent=None):
        super().__init__(parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        self.bundle = InstrumentBundle() if bundle is None else bundle

        self.data = DataHandler(self.bundle.get_header())
        self.data.dataRefreshed.connect(self.ui.dataview.model().set_dframe)
        self.data.dataRefreshed.connect(self.update_graph)
        self.ui.pbGraphReload.clicked.connect(self.update_graph)

        self.thread = QtCore.QThread(self)
        self.comrunner = CommandRunner(self.bundle)
        self.comrunner.moveToThread(self.thread)
        self.comrunner.dataPointObtained.connect(self.data.append_row)
        self.comrunner.commandStarted.connect(
            lambda i, n, com: self.ui.leCurrentCommand.setText('%d/%d: %s' % (i+1, n, com))
        )
        self.comrunner.commandFinished.connect(self.check_command_status)
        self.comrunner.allFinished.connect(
            lambda: QtWidgets.QMessageBox.information(self, 'all finished', 'all commands finished.')
        )
        self.comrunner.allFinished.connect(lambda: self.ui.pbRun.setEnabled(True))
        self.comrunner.allFinished.connect(lambda: self.ui.pbAbort.setEnabled(False))

        self.ui.pbDataFile.clicked.connect(self.set_datafile)
        self.ui.pbAbort.clicked.connect(self.abort_measurements)
        self.ui.pbRun.clicked.connect(lambda: QtCore.QMetaObject.invokeMethod(
            self.comrunner, 'run_all', QtCore.Q_ARG(object, self.ui.editor.command_list)
        ))
        self.ui.pbRun.clicked.connect(lambda: self.ui.pbRun.setEnabled(False))
        self.ui.pbRun.clicked.connect(lambda: self.ui.pbAbort.setEnabled(True))

        self.thread.start()

    def set_datafile(self, *, f=None):
        if f is None:
            f, _ = QtWidgets.QFileDialog.getSaveFileName(self, 'select file', '', '*.dat;; *')
            f = QtCore.QDir.toNativeSeparators(f)
        if f:
            with catcher(Exception, 'failed to open file: %s' % f, logger):
                self.data.open_file(f)
            self.ui.leDataFile.setText(str(f))
            self.ui.pbRun.setEnabled(True)

    def check_command_status(self, i, N, command, exc_info):
        if exc_info[0] is not None:
            exc = ''.join(traceback.format_exception(*exc_info))
            msg = """Command %s (%d/%d) failed due to following error:
            
------------------------------------------------
%s
------------------------------------------------""" % (command, i+1, N, exc)
            QtWidgets.QMessageBox.warning(self, 'command failed', msg)

    def abort_measurements(self):
        if QtWidgets.QMessageBox.information(
                self, 'confirmation', 'Abort all measurements?', QtWidgets.QMessageBox.No | QtWidgets.QMessageBox.Yes
        ) == QtWidgets.QMessageBox.Yes:
            self.ui.pbAbort.setEnabled(False)
            QtCore.QTimer.singleShot(0, self.comrunner.abort)

    def update_graph(self):
        """グラフのタブが開かれているときだけ、グラフを再描画する。"""
        if self.ui.graphview.isVisible():
            if self.ui.cbGraphAutoUpdate.isChecked() or self.sender() is self.ui.pbGraphReload:
                self.ui.graphview.load_data(self.data.dataframe)

    def closeEvent(self, event):
        if self.comrunner.is_running:
            QtWidgets.QMessageBox.warning(self, '', 'Please abort measurements before close')
            event.ignore()
        elif QtWidgets.QMessageBox.information(
                self, 'confirmation', 'Exit?', QtWidgets.QMessageBox.No | QtWidgets.QMessageBox.Yes
        ) == QtWidgets.QMessageBox.Yes:
            event.accept()
        else:
            event.ignore()


if __name__ == '__main__':
    import sys
    app = QtWidgets.QApplication(sys.argv)
    m = MainWindow()
    m.show()
    sys.exit(app.exec_())
