# -*- coding: utf-8 -*-
import typing
from logging import getLogger
from copy import copy
from PyQt5 import QtWidgets, QtCore

from fieldeffect.command import CommandList, CommandBase, SetSourceDrain
from .ui.ui_sequenceeditor import Ui_SequenceEditor
from .ui.ui_overridedialog import Ui_OverrideDialog
from ..models import PyListModel
from ..misc import catcher

logger = getLogger(__name__)


class SequenceEditor(QtWidgets.QWidget):
    DEFAULT = [SetSourceDrain(mode='voltage', level=0.1, compliance=0.01, four_wire=False)]

    def __init__(self, parent=None):
        super().__init__(parent)
        self.ui = Ui_SequenceEditor()
        self.ui.setupUi(self)
        self.listModel = PyListModel(self.DEFAULT)  # type: typing.List[CommandBase]
        self.ui.lvCommands.setModel(self.listModel)
        for i in range(self.ui.tabWidget.count()):
            for le in self.ui.tabWidget.widget(i).findChildren(QtWidgets.QLineEdit):
                le.returnPressed.connect(lambda: self.manipulate_command(action='append'))
        for i in range(self.ui.glCommandButtons.count()):
            self.ui.glCommandButtons.itemAt(i).widget().clicked.connect(self.manipulate_command)
        self.ui.pbSave.clicked.connect(self.save_sequence)
        self.ui.pbLoad.clicked.connect(self.load_sequence)
        self.ui.lvCommands.doubleClicked.connect(self.update_inputs)

    def save_sequence(self, *, f=None):
        if f is None:
            f, _ = QtWidgets.QFileDialog.getSaveFileName(self, 'select file', '', '*.json;;*')
        if f:
            with catcher(Exception, 'Cannot save sequence as %s' % f, logger):
                with open(f, 'w', encoding='utf-8') as fp:
                    self.command_list.dump_json(fp)
                QtWidgets.QMessageBox.information(self, 'file saved.', 'sequence successfully saved as:\n%s' % f)

    def load_sequence(self, *, f=None):
        if f is None:
            f, _ = QtWidgets.QFileDialog.getOpenFileName(self, 'select file', '', '*.json;;*')
        if f:
            com_new = CommandList()
            with catcher(Exception, 'Cannot load sequence from %s' % f, logger):
                with open(f, encoding='utf-8') as fp:
                    com_new.from_json(fp)
            if com_new:
                dialog = QtWidgets.QDialog(self)
                dialog.ui = Ui_OverrideDialog()
                dialog.ui.setupUi(dialog)
                ret = dialog.exec_()
                if ret == QtWidgets.QDialog.Accepted:
                    if dialog.ui.rbAppend.isChecked():
                        self.listModel.extend(com_new)
                    elif dialog.ui.rbOverride.isChecked():
                        self.listModel[:] = com_new

    def update_inputs(self, index: QtCore.QModelIndex):
        try:
            command = self.listModel[index.row()]
        except IndexError:
            return
        self.ui.tabWidget.currentWidget().update_from_command(command)

    def manipulate_command(self, *, action=None):
        sender = self.sender()
        com = self.ui.tabWidget.currentWidget().as_command()
        index = self.ui.lvCommands.currentIndex()
        row = index.row()
        if sender is self.ui.pbClear or action == 'clear':
            if QtWidgets.QMessageBox.information(
                    self, 'confirmation', 'Clear all commands. Ok?',
                            QtWidgets.QMessageBox.Cancel | QtWidgets.QMessageBox.Ok
            ) == QtWidgets.QMessageBox.Ok:
                self.listModel[:] = self.DEFAULT
        elif sender is self.ui.pbAppend or action == 'append':
            if com is not None:
                self.listModel.append(com)
                self.ui.lvCommands.setCurrentIndex(self.listModel.index(len(self.listModel)-1))
        elif not 0 <= row < len(self.listModel):
            return
        elif sender is self.ui.pbInsert or action == 'insert':
            if com is not None:
                self.listModel.insert(row, com)
        elif sender is self.ui.pbReplace or action == 'replace':
            if com is not None:
                self.listModel[row] = com
        elif sender is self.ui.pbDuplicate or action == 'duplicate':
            self.listModel.insert(row, copy(self.listModel[row]))
        elif sender is self.ui.pbDelete or action == 'delete':
            del self.listModel[row]
        elif sender is self.ui.pbMoveUp or action == 'move_up':
            self.listModel.moveRow(QtCore.QModelIndex(), row, QtCore.QModelIndex(), row - 1)
        elif sender is self.ui.pbMoveDown or action == 'move_down':
            self.listModel.moveRow(QtCore.QModelIndex(), row, QtCore.QModelIndex(), row + 2)
        elif action:
            raise ValueError('Unknown action: %s' % action)

    @property
    def command_list(self):
        return CommandList(self.listModel)


def main():
    import logging
    logging.basicConfig(format='%(levelname)s:%(message)s')
    app = QtWidgets.QApplication([])
    from fieldeffect.utils import func_tracer
    PyListModel.setData = func_tracer(PyListModel.setData)
    m = SequenceEditor()
    m.show()
    app.exec_()


if __name__ == '__main__':
    main()

