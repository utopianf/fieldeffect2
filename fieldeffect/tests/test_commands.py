# -*- coding: utf-8 -*-
# -*- coding: utf-8 -*-
import unittest
import random
import time
from itertools import product

import numpy as np

from slave.transport import SimulatedTransport

from fieldeffect import config, logger, command, driver
from fieldeffect.measurement import SecondChannel


class TestSetups(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.bundle = bundle
        cls.ppms = type(cls.bundle.ppms)(cls.bundle.ppms._transport)
        cls.sd = type(cls.bundle.sd)(cls.bundle.sd._transport)
        if isinstance(cls.bundle.gate, SecondChannel):
            cls.gate = SecondChannel(cls.sd)
        else:
            cls.gate = type(cls.bundle.gate)(cls.bundle.gate._transport)
        cls.voltmeters = [type(v)(v._transport) for v in cls.bundle.voltmeters]
        cls.bundle.clear_all()

    @classmethod
    def tearDownClass(cls):
        cls.bundle.sd.output.state = False
        cls.bundle.gate.output.state = False

    def test_setups(self):
        for v in self.voltmeters:
            self.assertEqual(v.sense.function, 'voltage')
            self.assertFalse(v.initiate.continuous_mode)
            self.assertEqual(v.triggering.count, 1)
            self.assertAlmostEqual(v.triggering.delay, config.TRIGGER_DELAY, delta=config.TRIGGER_DELAY*0.01)
            if hasattr(v.sense, 'voltage'):
                self.assertAlmostEqual(v.sense.voltage.nplc, config.NPLC, delta=config.NPLC*0.01)
            else:
                self.assertAlmostEqual(v.sense.nplc, config.NPLC, delta=config.NPLC*0.01)

        self.assertEqual(self.gate.source.mode, 'voltage')

        if isinstance(self.gate, SecondChannel):
            smus = (self.sd,)
        else:
            smus = (self.sd, self.gate)
        for smu in smus:
            with self.subTest('sd' if smu is self.sd else 'gate'):
                self.assertTupleEqual(tuple(smu.sense_elements), ("voltage", "current", "resistance"))
                self.assertSetEqual(set(smu.sense.functions), {"resistance", "current", "voltage"})
                self.assertEqual(smu.sense.res.mode, 'manual')
                self.assertAlmostEqual(smu.sense.voltage.nplc, config.NPLC, delta=config.NPLC/100)
                self.assertAlmostEqual(smu.sense.current.nplc, config.NPLC, delta=config.NPLC/100)

    def test_measure(self):
        self.bundle.gate.source.voltage.level = 0
        self.bundle.gate.source.voltage.level_triggered = 0
        for state in (True, False, True, False):
            with self.subTest('Gate output: %s' % state):
                self.bundle.gate.output.state = state
                self.bundle.sd.initiate()
                self.bundle.sd.wait_to_continue()
                self.bundle.gate.initiate()
                self.bundle.gate.wait_to_continue()
                data_sd = self.bundle.sd.fetch()
                data_gate = self.bundle.gate.fetch()
                self.assertEqual(len(data_sd), 3, msg=data_sd)
                self.assertEqual(len(data_gate), 3, msg=data_gate)


class TestCommands(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.bundle = bundle
        cls.ppms = type(bundle.ppms)(bundle.ppms._transport)
        cls.sd = type(bundle.sd)(bundle.sd._transport)
        if isinstance(bundle.gate, SecondChannel):
            cls.gate = SecondChannel(cls.sd)  # type: driver.B2900
        else:
            cls.gate = type(bundle.gate)(bundle.gate._transport)
        cls.voltmeters = [type(v)(v._transport) for v in bundle.voltmeters]

    @classmethod
    def tearDownClass(cls):
        cls.bundle.ppms.set_field(0, 100, wait_for_stability=False)
        cls.bundle.sd.output.state = False
        cls.bundle.gate.output.state = False

    def test_set_sd(self):
        # voltage
        for level, limit, fw in product((-1, 1), (1e-3, 1e-6), (True, False)):
            with self.subTest(mode='voltage', level=level, compliance=limit, four_wire=fw):
                com = command.SetSourceDrain('voltage', level, limit, fw)
                com.run(self.bundle, lambda: None)
                self.assertEqual(self.sd.sense.four_wire, fw)
                self.assertAlmostEqual(self.sd.sense.current.compliance, limit, delta=0.1*limit)
                self.assertAlmostEqual(self.sd.source.voltage.level, level, delta=level*0.1)
                self.assertAlmostEqual(self.sd.source.voltage.level_triggered, level, delta=level*0.1)
                self.assertEqual(self.sd.source.mode, 'voltage')
        # current
        for level, limit, fw in product((-1e-3, 1e-3), (1, 10), (True, False)):
            with self.subTest(mode='current', level=level, compliance=limit, four_wire=fw):
                com = command.SetSourceDrain('current', level, limit, fw)
                com.run(self.bundle, lambda: None)
                self.assertEqual(self.sd.sense.four_wire, fw)
                self.assertAlmostEqual(self.sd.sense.voltage.compliance, limit, delta=0.1*limit)
                self.assertAlmostEqual(self.sd.source.current.level, level, delta=level*0.1)
                self.assertAlmostEqual(self.sd.source.current.level_triggered, level, delta=level*0.1)
                self.assertEqual(self.sd.source.mode, 'current')

    def test_scan_iv(self):
        # voltage
        for mode, val, fw, comp in product(('voltage',), (1, -0.1), (True, False), (1e-4, 1e-3)):
            with self.subTest(mode=mode, target=val, four_wire=fw, compliance=comp):
                com = command.ScanIV(-val, val, 11, mode, comp, fw,  False)
                com.run(self.bundle, lambda: None)
                self.assertAlmostEqual(self.sd.source.voltage.level, val, delta=val*0.05)
                self.assertAlmostEqual(self.sd.source.voltage.level_triggered, val, delta=val*0.05)
                self.assertEqual(self.sd.source.mode, mode)
                self.assertAlmostEqual(self.sd.sense.current.compliance, comp, delta=0.2*comp)
        for mode, val, fw, comp in product(('current',), (1e-4, -1e-2), (True, False), (1, 10)):
            with self.subTest(mode=mode, target=val, four_wire=fw, compliance=comp):
                com = command.ScanIV(-val, val, 11, mode, comp, fw,  False)
                com.run(self.bundle, lambda: None)
                self.assertAlmostEqual(self.sd.source.current.level, val, delta=val*0.05)
                self.assertAlmostEqual(self.sd.source.current.level_triggered, val, delta=val*0.05)
                self.assertEqual(self.sd.source.mode, mode)
                self.assertAlmostEqual(self.sd.sense.voltage.compliance, comp, delta=0.2*comp)

    def test_scan_iv_return(self):
        for mode, gateon in product(('voltage', 'current'), (True, False)):
            for i in range(2):
                with self.subTest(mode=mode, try_=i):
                    self.bundle.gate.output.state = gateon
                    mode_before = 'current' if mode == 'voltage' else 'voltage'
                    level_before = np.random.uniform(-1, 1) * 0.1
                    compl_before = max(np.random.random(), 0.01)
                    self.sd.source.function_mode = mode_before
                    getattr(self.sd.source, mode_before).level = level_before
                    getattr(self.sd.source, mode_before).level_triggered = level_before
                    getattr(self.sd.sense, mode).compliance = compl_before
                    com = command.ScanIV(-1e-3, 1e-3, 3, mode, 1, False, go_back=True)
                    com.run(self.bundle, lambda: None)
                    self.assertEqual(self.gate.output.state, gateon)
                    self.assertEqual(self.sd.source.function_mode, mode_before)
                    self.assertAlmostEqual(getattr(self.sd.source, mode_before).level, level_before, delta=abs(0.01*level_before))
                    self.assertAlmostEqual(getattr(self.sd.source, mode_before).level_triggered, level_before, delta=abs(0.01*level_before))
                    self.assertAlmostEqual(getattr(self.sd.sense, mode).compliance, compl_before, delta=0.01*compl_before)

    def test_scan_temperature(self):
        if isinstance(self.ppms._transport, SimulatedTransport):
            logger.warning('PPMS is not connected. Skipping test_scan_temperature')
            return
        for mode in ('no overshoot', 'fast'):
            # 現在温度 ±10K, ただし10から300Kまでの間
            target = min(
                max(10, (self.ppms.temperature + random.uniform(-10, 10))),
                300
            )
            with self.subTest(target=target, mode=mode):
                com = command.ScanTemperature(target, 10, 10, mode)
                com.run(self.bundle, lambda: None)
                self.assertAlmostEqual(self.ppms.temperature, target, delta=0.1)

    def test_scan_field(self):
        if isinstance(self.ppms._transport, SimulatedTransport):
            logger.warning('PPMS is not connected. Skipping test_scan_field')
            return
        target = (500, -500, 0)
        opts = ('linear', 'no overshoot', 'oscillate')
        for t, o in zip(target, opts):
            with self.subTest(target=t, option=o):
                com = command.ScanField(t, 100, 10, o, 'persistent')
                com.run(self.bundle, lambda: None)
                self.assertAlmostEqual(self.ppms.field, t, delta=5)

    def test_wait(self):
        com = command.Wait(total=10, interval=1)
        start = time.perf_counter()
        com.run(self.bundle, lambda: time.sleep(np.random.random()*0.5))
        end = time.perf_counter()
        self.assertAlmostEqual(end - start, 10, delta=1)

    def test_data_retrieval(self):
        with self.subTest('without inversion'):
            result = self.bundle.measure()
            self.assertEqual(len(result), len(self.bundle.get_header()), msg='result: %r' % result)
            for r in result:
                self.assertIsInstance(r, float)
        with self.subTest('with inversion'):
            level = 1
            com = command.SetSourceDrain(mode='voltage', level=level, compliance=1, four_wire=False)
            com.run(self.bundle, lambda: None)
            r1, r2 = self.bundle.measure_bipolar()
            idx_v = self.bundle.get_header().index('V_SD')
            V_SD_1 = r1[idx_v]
            V_SD_2 = r2[idx_v]
            self.assertAlmostEqual(V_SD_1, level, delta=0.05*level)
            self.assertAlmostEqual(V_SD_2, -level, delta=0.05*level)

    def test_shutdown(self):
        for ppms, sd, gate in product((True, False,), (True, False), (True, False)):
            with self.subTest(stop_sd=sd, stop_gate=gate):
                com = command.Shutdown(stop_ppms=ppms, stop_sd=sd, stop_gate=gate)
                com.run(self.bundle, lambda: None)
                self.assertEqual(self.sd.output.state, not sd)
                self.assertEqual(self.gate.output.state, not gate)
                self.bundle.gate.output.state = True
                self.bundle.sd.output.state = True
            if isinstance(self.ppms._transport, SimulatedTransport):
                logger.warning('PPMS is not connected. Skipping part of test_shutdown')
            else:
                if ppms:
                    self.assertEqual(self.ppms.system_status['temperature'], 'standby mode invoked')

    def test_apply_vg(self):
        self.gate.source.voltage.level = 0
        self.gate.source.voltage.level_triggered = 0
        for fw, target in product((True, False), (1.0, -1.0, 0.0)):
            with self.subTest(four_wire=fw, target=target):
                step, interval = 0.2, 0.5
                com = command.ApplyGateVoltage(target=target, interval=interval, step=step, is_3e=fw, compliance=1e-2)
                t = (abs(target - self.gate.source.voltage.level) // step + 1) * interval
                t_before = time.perf_counter()
                com.run(self.bundle, lambda: None)
                t_after = time.perf_counter()
                self.assertAlmostEqual(self.gate.sense.current.compliance, 1e-2, 2e-3)
                self.assertAlmostEqual(self.gate.source.voltage.level, target, 0.01)
                self.assertAlmostEqual(self.gate.source.voltage.level_triggered, target, 0.01)
                self.assertEqual(self.gate.sense.four_wire, fw)
                self.assertTrue(self.gate.output.state)
                ret = self.bundle.measure()
                vg = ret[self.bundle.get_header().index('V_G')]
                self.assertAlmostEqual(vg, target, delta=0.05)
                self.assertAlmostEqual(t_after - t_before, t, delta=1)


def main(verbosity=2):
    import logging
    logging.basicConfig()
    logger.setLevel('DEBUG')
    print('-'*80 + '\n')
    print('configurations in config.py:\n')
    print(*('    %s = %r' % (k, getattr(config, k)) for k in dir(config) if not k.startswith('_')), sep='\n')
    print('\n' + '-'*80)

    from PyQt5 import QtWidgets
    from fieldeffect.qt.widgets.connectiondialog import ConnectionDialog
    app = QtWidgets.QApplication([])
    cd = ConnectionDialog()
    if cd.exec_() == QtWidgets.QDialog.Accepted:
        global bundle
        bundle = cd.bundle
    else:
        import sys
        sys.exit(1)
    unittest.main(__name__, verbosity=verbosity)


if __name__ == '__main__':
    main()
