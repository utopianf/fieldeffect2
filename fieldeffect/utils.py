# -*- coding: utf-8 -*-
import logging
import time
from itertools import count
from functools import wraps

logger = logging.getLogger(__name__)


def retry(catch, exclude=tuple(), n=10, wait=1, maxwait=float('inf'), backoff=1, logger=logger, onerror=None):
    def on_deco(f):
        @wraps(f)
        def on_call(*a, **kw):
            for i in range(n):
                try:
                    return f(*a, **kw)
                except catch as e:
                    if (i == n - 1) or isinstance(e, exclude):
                        raise
                    if onerror is not None:
                        onerror()
                    t = min((wait * backoff**i, maxwait))
                    ename = '.'.join([e.__class__.__module__, e.__class__.__name__])
                    logger.info('%r raised %s. Retry in %.2f s', f, ename, t)
                    logger.debug('', exc_info=True)
                    time.sleep(t)
        return on_call
    return on_deco


def flatten(*args):
    for a in args:
        if isinstance(a, (str, bytes, bytearray)) or not hasattr(a, '__iter__'):
            yield a
        else:
            yield from flatten(*a)


def tick(t, N=None, logger=logger):
    t0 = time.perf_counter()
    for i in count():
        yield i
        if N and (i+1) >= N:
            break
        wait = t0 + (i+1)*t - time.perf_counter()
        if wait < 0:
            logger.warning('tick delay by %.2f s', -wait)
            logger.debug('', stack_info=True)
        else:
            time.sleep(wait)


def func_tracer(fn):
    """Traces every function call"""
    count = 1
    try:
        logname = getattr(fn, '__module__') + '.' + fn.__name__
    except (AttributeError, TypeError):
        logname = __name__ + '.' + fn.__name__
    logger = logging.getLogger(logname)
    logger.setLevel('DEBUG')

    @wraps(fn)
    def inner(*args, **kwargs):
        nonlocal count
        logger.debug('-'*40)
        logger.debug('Call #%d to %s', count, fn.__name__)
        count += 1

        repr_args = [repr(a) for a in args]
        repr_kwargs = {n: repr(a) for n, a in kwargs.items()}
        if args:
            logger.debug('  args:    %s', ',\n    '.join(repr_args))
        if kwargs:
            logger.debug('  kwargs:    %s', ',\n    '.join('%s=%s' % (k, v) for k, v in repr_kwargs.items()))

        ret = fn(*args, **kwargs)
        logger.debug('  return: %r', ret)

        return ret

    return inner


def method_tracer(cls):
    """Trace every method call in a class"""
    try:
        logger = logging.getLogger(getattr(cls, '__module__', __name__) + '.' + cls.__name__)
    except TypeError:
        logger = logging.getLogger(__name__ + '.' + cls.__name__)
    logger.setLevel('DEBUG')

    def trace(fn, attrname):
        count = 0

        @wraps(fn)
        def inner(*args, **kwargs):
            nonlocal count
            count += 1
            logger.debug('Call #%d to %s.%s', count, cls.__name__, attrname)
            return fn(*args, **kwargs)
        return inner

    for attr, val in cls.__dict__.items():
        if hasattr(val, '__call__'):
            setattr(cls, attr, trace(val, attr))
    return cls
