# -*- coding: utf-8 -*-
from slave import iec60488 as iec
from slave.driver import Driver, Command
from slave.types import Mapping, Float, Stream, Boolean


class Sense(Driver):
    def __init__(self, transport, protocol=None, *args, **kw):
        super(Sense, self).__init__(transport, protocol, *args, **kw)
        self.voltage = SenseVoltage(transport, protocol)
        self.function = Command(':SENS:FUNC?', ':SENS:FUNC', Mapping({'voltage': '"VOLT:DC"'}))


class SenseVoltage(Driver):
    def __init__(self, transport, protocol=None):
        super(SenseVoltage, self).__init__(transport, protocol)
        self.nplc = Command(':SENS:VOLT:NPLC?', ':SENS:VOLT:NPLC', Float)


class Triggering(Driver):
    def __init__(self, transport, protocol=None):
        super(Triggering, self).__init__(transport, protocol)
        self.continuous_mode = Command(':INIT:CONT?', ':INIT:CONT', Boolean)
        self.delay = Command(':TIRG:DEL?', ':TRIG:DEL', Float)


class Initiate(Driver):
    def __init__(self, transport, protocol=None):
        super(Initiate, self).__init__(transport, protocol)
        self.continuous = self.continuous_mode = Command(':INIT:CONT?', ':INIT:CONT', Boolean)

    def __call__(self):
        self._write(':INIT')


class ProtoVoltmeter(iec.IEC60488, iec.Trigger, iec.StoredSetting):
    def __init__(self, transport, protocol=None):
        super(ProtoVoltmeter, self).__init__(transport, protocol)
        self.sense = Sense(transport, protocol)
        self.triggering = Triggering(transport, protocol)
        self.initiate = Initiate(transport, protocol)

    def fetch(self):
        return self._query(('FETC?', Stream(Float)))


if __name__ == '__main__':
    from slave.transport import Visa
    import sys
    if input('Hit ENTER to start tests. Input something to abort.'):
        sys.exit(1)
    import visa
    rm = visa.ResourceManager()
    print('Available resources:')
    print(*rm.list_resources(), sep='\n')
    addr = input('Enter address: ')

    def testattr(obj, attr, val):
        object.__getattribute__(obj, attr)
        setattr(obj, attr, val)
        assert getattr(obj, attr) == val

    voltmeter = ProtoVoltmeter(Visa(addr))

    testattr(voltmeter.sense, 'function', 'voltage')
    testattr(voltmeter.sense.voltage, 'nplc', 1.0)

    testattr(voltmeter.triggering, 'continuous_mode', False)
    testattr(voltmeter.triggering, 'delay', 0)

    voltmeter.initiate()
    voltmeter.wait_to_continue()
    v = voltmeter.fetch()
    if hasattr(v, '__iter__'):
        assert len(v) == 1 and isinstance(v[0], float)
    else:
        assert isinstance(v, float)
