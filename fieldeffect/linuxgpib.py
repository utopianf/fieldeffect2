# -*- coding: utf-8 -*-
import ctypes as ct
import ctypes.util

from slave.types import Register
from slave.transport import Transport, TransportError, Timeout


class LinuxGpib(Transport):
    """A linux-gpib adapter (adopted with fixes from https://github.com/p3trus/slave) .
    E.g.::
        transport = LinuxGpib(primary=11, timeout='1 s')
        with transport:
            transport.write(b'*IDN?\\n')
            idn = transport.read_until(b'\\n')
        transport.close()
    :param int primary: The primary gpib address.
    :param int secondary: The secondary gpib address. An integer in the range 0
        to 30 or `None`. `None` disables the use of secondary addressing.
    :param int board: The gpib board index.
    :param timeout: The timeout for IO operations. See :attr:`LinuxGpib.TIMEOUT`
        for possible values.
    :param bool send_eoi: If `True`, the EOI line will be asserted with the last
        byte sent during write operations.
    :param str eos_char: End of string character.
    :param int eos_mode: End of string mode.
    """
    #: Valid timeout parameters.
    TIMEOUT = [
        None, '10 us', '30 us', '100 us', '300 us', '1 ms', '3 ms',
        '10 ms', '30 ms', '100 ms', '300 ms', '1 s', '3 s', '10 s', '30 s',
        '100 s', '300 s', '1000 s'
    ]
    #: Enable termination of reads when eos character is received.
    REOS = 0x400
    #: Assert the EOI line whenever the eos character is sent during writes.
    XEOS = 0x800
    #: Match eos character using all 8 bits instead of the 7 least significant bits.
    BIN = 0x1000

    #: Possible error messages.
    ERRNO = {
        0: 'A system call has failed.',
        1: 'Your interface needs to be controller-in-charge, but is not.',
        2: 'You have attempted to write data or command bytes, but there are no listeners currently addressed.',
        3: 'The interface board has failed to address itself properly before starting an io operation.',
        4: 'One or more arguments to the function call were invalid.',
        5: 'The interface board needs to be system controller, but is not.',
        6: 'A read or write of data bytes has been aborted, possibly due to a timeout or reception of a device clear command.',
        7: 'The GPIB interface board does not exist, its driver is not loaded, or it is not configured properly.',
        8: 'Not used (DMA error), included for compatibility purposes.',
        10: 'Function call can not proceed due to an asynchronous IO operation (ibrda(), ibwrta(), or ibcmda()) in progress.',
        11: 'Incapable of executing function call, due the GPIB board lacking the capability, or the capability being disabled in software.',
        12: 'File system error. ibcnt/ibcntl will be set to the value of errno.',
        14: 'An attempt to write command bytes to the bus has timed out.',
        15: 'One or more serial poll status bytes have been lost.',
        16: 'The serial poll request service line is stuck on.',
        20: 'ETAB'
    }

    #: Status Register. Please consult the linux-gpib manual for a description.
    STATUS = {
        0: 'device clear',
        1: 'device trigger',
        2: 'listener',
        3: 'talker',
        4: 'atn',
        5: 'controller-in-charge',
        6: 'remote',
        7: 'lockout',
        8: 'io complete',
        9: 'event',
        10: 'serial polled',
        11: 'srq',
        12: 'eoi',
        13: 'timeout',
        14: 'error'
    }

    class Error(TransportError):
        """Generic linux-gpib error."""

    class Timeout(Error, Timeout):
        """Raised when a linux-gpib timeout occurs."""

    def __init__(self, primary=0, secondary=None, board=0, timeout='10 s',
                 send_eoi=True, eos_char=None, eos_mode=0):
        super(LinuxGpib, self).__init__()

        valid_address = list(range(0, 31))
        if primary not in valid_address:
            raise ValueError('Primary address must be in the range 0 to 30.')

        if secondary not in (valid_address + [None]):
            raise ValueError('Secondary address must be in the range 0 to 30 or None')
        # Linux-gpib secondary addresses need an offset of 96 following NI
        # convention.
        secondary = 0 if secondary is None else secondary + 96
        timeout = self.TIMEOUT.index(timeout)
        send_eoi = bool(send_eoi)

        if not eos_mode in [0, LinuxGpib.REOS, LinuxGpib.XEOS, LinuxGpib.BIN]:
            raise ValueError('Invalid eos_mode')

        if eos_char is None:
            eos = eos_mode
        else:
            eos = ord(eos_char) + eos_mode

        self._lib = ct.CDLL(ct.util.find_library('gpib'))
        self._device = self._lib.ibdev(
            ct.c_int(board), ct.c_int(primary), ct.c_int(secondary),
            ct.c_int(timeout), ct.c_int(send_eoi), ct.c_int(eos)
        )
        if self._device == -1:
            raise self.Error('failed to open LinuxGpib device')
        self._ibsta_parser = Register(LinuxGpib.STATUS)

    def __del__(self):
        self.close()

    def close(self):
        """Closes the gpib transport."""
        if self._device not in (None, -1):
            ibsta = self._lib.ibonl(self._device, 0)
            self._check_status(ibsta)
            self._device = None

    def __write__(self, data):
        d = ct.c_char_p(bytes(data))
        ibsta = self._lib.ibwrt(self._device, ct.cast(d, ct.c_void_p), ct.c_long(len(data)))
        self._check_status(ibsta)

    def __read__(self, num_bytes):
        buffer = ct.create_string_buffer(num_bytes)
        ibsta = self._lib.ibrd(self._device, ct.byref(buffer), ct.c_long(num_bytes))
        self._check_status(ibsta)
        return buffer.value

    def clear(self):
        """Issues a device clear command."""
        ibsta = self._lib.ibclr()
        self._check_status(ibsta)

    def trigger(self):
        """Triggers the device.
        The trigger method sens a GET(group execute trigger) command byte to
        the device.
        """
        ibsta = self._lib.ibtrg(self._device)
        self._check_status(ibsta)

    @property
    def status(self):
        ibsta = self._lib.ThreadIbsta()
        return self._ibsta_parser.load(ibsta)

    @property
    def error_status(self):
        error = self._lib.ThreadIberr()
        return LinuxGpib.ERRNO[error]

    def _check_status(self, ibsta):
        """Checks ibsta value."""
        if ibsta & 0x4000:
            raise LinuxGpib.Timeout()
        elif ibsta & 0x8000:
            raise LinuxGpib.Error(self.error_status)
