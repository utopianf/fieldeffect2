# -*- coding: utf-8 -*-
import json
import time
from logging import getLogger

import numpy as np
from slave.transport import SimulatedTransport
from . import config
from .utils import tick

logger = getLogger(__name__)


class CommandList(list):
    def dump_json(self, fp):
        obj = [{
                'cls': com.__class__.__name__,
                'parameters': com.parameters
                } for com in self]
        json.dump(obj, fp, indent=2, sort_keys=True)
    
    def from_json(self, fp, append=True):
        obj = json.load(fp)
        if not append:
            self[:] = []
        for o in obj:
            comcls = globals()[o['cls']]
            kwargs = o['parameters']
            self.append(comcls(**kwargs))
        return self


class CommandBase:
    def __init__(self, **kwargs):
        self._parameters = kwargs
        for k, v in kwargs.items():
            setattr(self, k, v)

    def to_dict(self):
        return {k: v for k, v in self._parameters.items()}

    def run(self, bundle, operation):
        """
        :type bundle: fieldeffect.measurement.InstrumentBundle
        :param operation: callable
        """
        raise NotImplementedError

    @property
    def parameters(self):
        return {k: v for k, v in self._parameters.items()}

    def __str__(self):
        name = self.__class__.__name__
        ps = ', '.join('%s=%r' % (k, v) for k, v in self._parameters.items())
        return name + '(' + ps + ')'

    def __copy__(self):
        p = self.parameters
        return type(self)(**p)


class Comment(CommandBase):
    def __init__(self, comment):
        super().__init__(comment=comment)

    def run(self, bundle, operation):
        pass


class Wait(CommandBase):
    def __init__(self, total, interval):
        super().__init__(total=total, interval=interval)

    def run(self, bundle, operation):
        t0 = time.perf_counter()
        for _ in tick(self.interval, logger=logger):
            operation()
            if time.perf_counter() - t0 > self.total:
                break


class ApplyGateVoltage(CommandBase):
    def __init__(self, target, interval, step, is_3e, compliance):
        super().__init__(target=target, interval=interval, step=step, is_3e=is_3e, compliance=compliance)

    def run(self, bundle, operation):
        """
        :type bundle: fieldeffect.measurement.InstrumentBundle
        :param operation: callable
        """
        bundle.gate.sense.four_wire = self.is_3e
        bundle.gate.sense.current.compliance = self.compliance
        bundle.gate.output.state = True
        v = bundle.gate.source.voltage.level
        if self.target > v:
            volts = np.arange(self.target, v, -np.abs(self.step))[::-1]
        else:
            volts = np.arange(self.target, v, np.abs(self.step))[::-1]
        if len(volts) == 0:
            volts = [self.target]
        for v, _ in zip(volts, tick(self.interval, logger=logger)):
            bundle.gate.source.voltage.level = v
            bundle.gate.source.voltage.level_triggered = v
            operation()


class ScanTemperature(CommandBase):
    def __init__(self, target, rate, interval, mode='fast'):
        super().__init__(target=target, interval=interval, rate=rate, mode=mode)

    def run(self, bundle, operation):
        """
        :type bundle: fieldeffect.measurement.InstrumentBundle
        :param operation: callable
        """
        if isinstance(bundle.ppms._transport, SimulatedTransport):
            logger.warning('PPMS is not connected. Skipping %r', self)
            return
        bundle.ppms.set_temperature(self.target, self.rate, self.mode, wait_for_stability=False)
        t0 = time.perf_counter()
        for _ in tick(self.interval, logger=logger):
            if (bundle.ppms.system_status['temperature'] == 'normal stability at target temperature' and
                    time.perf_counter() - t0 > 10):
                break
            operation()


class ScanField(CommandBase):
    def __init__(self, target, rate, interval, approach='linear', mode='persistent'):
        """

        :param target: target field in Oe
        :param rate: in Oe/s
        :param interval: measure every x seconds
        :param approach:  'linear', 'no overshoot', or 'oscillate'
        :param mode: 'persistent' or 'driven'
        """
        super().__init__(target=target, interval=interval, approach=approach, rate=rate, mode=mode)

    def run(self, bundle, operation):
        """
        :type bundle: fieldeffect.measurement.InstrumentBundle
        :param operation: callable
        """
        if isinstance(bundle.ppms._transport, SimulatedTransport):
            logger.warning('PPMS is not connected. Skipping %r', self)
            return
        bundle.ppms.set_field(self.target, self.rate, self.approach, self.mode, wait_for_stability=False)
        if bundle.ppms.system_status['magnet'].startswith('persist'):
            operation()
            time.sleep(bundle.ppms.magnet_config[5])
        for _ in tick(self.interval, logger=logger):
            status = bundle.ppms.system_status['magnet']
            if status in ('persistent, stable', 'driven, stable'):
                break
            operation()


class Shutdown(CommandBase):
    def __init__(self, stop_ppms=False, stop_sd=False, stop_gate=False):
        super().__init__(stop_ppms=stop_ppms, stop_sd=stop_sd, stop_gate=stop_gate)

    def run(self, bundle, operation):
        """
        :type bundle: fieldeffect.measurement.InstrumentBundle
        :param operation: callable
        """
        if self.stop_ppms:
            bundle.ppms.shutdown()
        if self.stop_sd:
            bundle.sd.output.state = False
        if self.stop_gate:
            bundle.gate.output.state = False


class SetSourceDrain(CommandBase):
    def __init__(self, mode, level, compliance, four_wire):
        """

        :param mode: function mode, 'voltage' or 'current'
        :param level: source level, in V or mA
        :param compliance: compliance, in A or V
        :param four_wire: set four-wire measurement.
        """
        super().__init__(mode=mode, level=level, compliance=compliance, four_wire=four_wire)

    def run(self, bundle, operation):
        """
        :type bundle: fieldeffect.measurement.InstrumentBundle
        :param operation: callable
        """
        if self.mode.lower().startswith('v'):
            bundle.sd.source.mode = 'voltage'
            bundle.sd.source.voltage.level = self.level
            bundle.sd.source.voltage.level_triggered = self.level
            bundle.sd.sense.current.compliance = self.compliance
        else:
            bundle.sd.source.mode = 'current'
            bundle.sd.source.current.level = self.level
            bundle.sd.source.current.level_triggered = self.level
            bundle.sd.sense.voltage.compliance = self.compliance
        bundle.sd.sense.four_wire = self.four_wire


class ScanIV(CommandBase):
    def __init__(self, start, stop, points,  mode, compliance, four_wire, go_back):
        """

        :param start: sweep start level, in V or mA
        :param stop: sweep stop level, in V or mA
        :param points: number of sweep points
        :param mode: function mode, 'voltage' or 'current'
        :param compliance: compliance level, in A or V
        :param four_wire: set four-wire mode
        :param go_back: return to the state before IV scan
        """
        super().__init__(start=start, stop=stop, points=points, mode=mode, four_wire=four_wire, go_back=go_back, compliance=compliance)
        self._saved = False

    def run(self, bundle, operation):
        """
        :type bundle: fieldeffect.measurement.InstrumentBundle
        :param operation: callable
        """
        invert_before = config.INVERT
        # temporarily disable current inversion
        config.INVERT = False
        if self.go_back and not self._saved:
            bundle.sd.save(1)
            self._saved = True
        try:
            bundle.sd.source.mode = self.mode
            bundle.sd.sense.four_wire = self.four_wire
            if self.mode.lower().startswith('v'):
                values = np.linspace(self.start, self.stop, self.points)
                subsource = bundle.sd.source.voltage
                bundle.sd.sense.current.compliance = self.compliance
            else:
                values = np.linspace(self.start, self.stop, self.points, dtype=float)
                bundle.sd.sense.voltage.compliance = self.compliance
                subsource = bundle.sd.source.current
            for v in values:
                subsource.level = v
                subsource.level_triggered = v
                bundle.sd.wait_to_continue()
                operation()
            if self.go_back:
                bundle.sd.recall(1)
                bundle.sd.wait_to_continue()
                self._saved = False
        finally:
            config.INVERT = invert_before
