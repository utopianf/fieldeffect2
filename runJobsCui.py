# -*- coding: utf-8 -*-
import sys
import logging
import argparse
import os
from os import path
from datetime import datetime
from PyQt5 import QtCore
import fieldeffect
from fieldeffect import config
from fieldeffect.qt.comrunner import CommandRunner, DataHandler
from fieldeffect.command import CommandList
from fieldeffect.measurement import InstrumentBundle

parser = argparse.ArgumentParser()
parser.add_argument('--simulate', help='simulation mode', action='store_true')
parser.add_argument('--ppms', '-p', help='PPMS address', type=str, default='GPIB0::15::INSTR')
parser.add_argument('--smu', '-s',  help='B2902A address', type=str, default='GPIB0::23::INSTR')
parser.add_argument('--voltmeters', '-v', help='Voltmeter addresses', type=str, nargs='*', default=tuple())
parser.add_argument('--noinvert', help='no current inversion', action='store_true')
parser.add_argument('jobfile', type=str, help='path to sequence (.json) file')

logging.basicConfig()
fieldeffect.logger.setLevel('INFO')

app = QtCore.QCoreApplication([])

arguments = parser.parse_args()
if arguments.noinvert:
    config.INVERT = False
print(arguments)
if input('Press ENTER to start. Input something to abort.: '):
    sys.exit()
bundle = InstrumentBundle()
if not arguments.simulate:
    bundle.connect(
        addrPpms=arguments.ppms,
        addrSD=arguments.smu,
        addrGate=arguments.smu,
        *arguments.voltmeters
    )

datafile = path.expanduser('~/Desktop/FieldEffect/{:%Y%m%d-%H%M%S}.dat'.format(datetime.now()))
print('Data will be stored in: %s' % datafile)
os.makedirs(path.dirname(datafile), exist_ok=True)
dhandler = DataHandler(bundle.get_header())
dhandler.open_file(datafile)
dhandler.print_to_screen()
runner = CommandRunner(bundle)
print(*bundle.get_header(), sep='\t')
runner.dataPointObtained.connect(dhandler.append_row)
runner.run_all(CommandList().from_json(open(arguments.jobfile)))
