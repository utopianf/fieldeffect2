# -*- coding: utf-8 -*-
# check required modules
from compileUi import compile_ui
from importlib import import_module

requires = ('slave', 'matplotlib', 'pandas', 'PyQt5', 'pyvisa')


def check_requires():
    missing = []
    for mod in requires:
        try:
            import_module(mod)
        except ImportError:
            missing.append(mod)
    if missing:
        import sys
        import tkinter
        from tkinter import messagebox
        tkinter.Tk().withdraw()
        msg = """Please install required libraries:
        
%s""" % ', '.join(missing)
        messagebox.showerror("Error", msg)
        sys.exit(msg)


print('application loading. please wait...')
check_requires()
# compile .ui files if modified
compile_ui()

from fieldeffect.qt import main
main.main()
